Simple calendar with events using javascript. 
It uses cookies and localStorage to keep track of the chosen date and the events.
At present all the events are stored in a single property so if you want to use it in the future I highly recommend you use a database.
The way it is right now almost every functionality needs for all the events to be fetched and cycled through.