/* Simple calendar with events.
 * Clicking on a day will display all of it's events.
 * Double clicking on a day will display the add new event form.
 * Double clicking on a event will give the option of changing it or deleting it.
 * All the necessary data is store either in cookies or localStorage
 *
 * localStorage: calendarEvents(all of the events),
 * calendarId(to auto generate the next Id for events)
 */

 var selectedEvent = null;
 var calendarYear = -1;
 var calendarMonth = -1;
 var calendarDay = null;

 window.onload = function init() {
 	calendar();
 };
 dayNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
 monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


 function calendar() {
 	adjustDate();
	//Some elements need to be erased between calendar() callings.
	//Like remembering the selected day. Or erasing old elements from the calendar event list.
	cleanElements();
	displayCalendar();
}

function adjustDate() {
	var date = new Date();
	if (calendarYear < 0) {
		calendarYear = date.getFullYear();
		calendarMonth = date.getMonth();
	}

	if (calendarMonth < 0) {
		calendarMonth = 11;
		calendarYear--;
	} else if (calendarMonth > 11) {
		calendarMonth = 0;
		calendarYear++;
	}
}

function displayCalendar(){
	var table = document.getElementById("calendar");
	table.innerHTML = generateTableHTML();
	//Show which days of the this month has events in them
	prepareDaysForEvents();
	//Hightlight today if it is the current month.
	today();
}

function cleanElements() {
	selectedDay = null;
	selectedEvent = null;
	//TODO
	var events = document.getElementById("calendarEvents");
	events.innerHTML = "";
}

function generateTableHTML() {
	var firstDay = new Date(calendarYear, calendarMonth, 1);
	var startingDay = firstDay.getDay();
	//Change startingDay so first day is monday not sunday.
	if (startingDay == 0) {
		startingDay = 6;
	} else {
		startingDay--;
	}
	var monthLength = daysInMonth(calendarMonth, calendarYear);
	var monthName = monthNames[calendarMonth];
	var html = '<table class="calendarTable">';

	html += '<tr class="navigationRow" ><th colspan="2">';
	html += '<button onclick="clickPrevMonth()">Previous</button> ';
	html += '</th>';

	html += '<th colspan="3">';
	html += monthName + "&nbsp;" + calendarYear;
	html += '</th>';

	html += '<th colspan="2">';
	html += '<button onclick="clickNextMonth()">Next</button> ';
	html += '</th></tr>';

	html += '<tr class="calendarHeader">';
	for (var i = 0; i <= 6; i++) {
		html += '<td class="calendarHeaderDay">';
		html += dayNames[i];
		html += '</td>';
	}
	html += '</tr>';

	var dayCount = 1;
	var rows = 0;
	while (dayCount < monthLength) {
		html += '<tr>';
		for (var day = 0; day <= 6; day++) {
			html += '<td class="calendarDay"';
			if (isDayOfGivenMonth(dayCount, monthLength, startingDay, day)) {
				//Add an id and onClick function on every valid day.
				html += 'id = "day' + dayCount + '"';
				html += 'onClick="clickDay(' + dayCount + ')"';
				html += 'ondblclick="clickAdd()"';
				html += '>';
				//The content of the cell is the day's number.
				html += dayCount;
				dayCount++;
			} else {
				html += '>';
			}
			html += '</td>';
		}
		html += '</tr>';
		rows++;
	}
	while(rows<6){
		html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		rows++
	}
	html += '<tr><td colspan="7">';
	html += '<button onclick="clickToday()">Today</button>';
	html += '</td></tr>';
	html += '</table>';
	return html;
}

function daysInMonth(month, year) {
	return new Date(year, month + 1, 0).getDate();
}

//Function that tells if the current day is in the current month.
//This gives us the nice empty cells which are supposed to belong to the previous
//and next months.
function isDayOfGivenMonth(dayCount, monthLength, firstDayOfTheMonth, currentDay) {
	if (dayCount <= monthLength) {
		if (dayCount == 1) {
			if (currentDay >= firstDayOfTheMonth) {
				return true;
			}
		} else {
			return true;
		}
	}
	return false;
}

//If looking at current month, highlight today.
function today() {
	var date = new Date();
	if (date.getMonth() == calendarMonth && date.getFullYear() == calendarYear) {
		var day = date.getDate();
		document.getElementById("day" + day).classList.add("today");
	}
}

function prepareDaysForEvents() {
	getCalendarEvents().forEach(function(item) {
		var d = item.startDate;
		if (d.getFullYear() == calendarYear && d.getMonth() == calendarMonth) {
			document.getElementById("day" + d.getDate()).classList.add("hasEvent");
		}
	});
}

function getCalendarEvents() {
	var array = [];
	var data = localStorage.getItem("calendarEvents");
	if (data) {
		var items = JSON.parse(data);
		items.forEach(function(item) {
			array.push(parseEventContent(item));
		});
	}
	return array;
}

function parseEventContent(obj) {
	return {
		id : parseInt(obj.id),
		startDate : new Date(obj.startDate),
		endDate : new Date(obj.endDate),
		text : obj.text
	};
}

function getNextId() {
	var data = localStorage.getItem("calendarId");
	if (data) {
		var id = parseInt(data);
		localStorage.setItem("calendarId", id + 1);
		return id;
	}
	localStorage.setItem("calendarId", 2);
	return 1;
}

function deleteFromCalendarEvent(id) {
	if (id) {
		var array = getCalendarEvents();
		for (var i = 0; i < array.length; i++) {
			if (array[i].id == id) {
				array.splice(i, 1);
			}
		}
		localStorage.setItem("calendarEvents", JSON.stringify(array));
	}
}

function updateCalendarEvent(event) {
	if (event) {
		var array = getCalendarEvents();
		for (var i = 0; i < array.length; i++) {
			if (array[i].id == event.id) {
				array[i].startDate = event.startDate;
				array[i].endDate = event.endDate;
				array[i].text = event.text;
			}
		}
		localStorage.setItem("calendarEvents", JSON.stringify(array));
	}
}

function getCalendarEventWithId(id) {
	var event = "";
	getCalendarEvents().some(function(item) {
		if (item.id == id) {
			event = item;
			return true;
		}
	});
	return event;
}

function addCalendarEvent(event) {
	if (event) {
		var array = getCalendarEvents();
		var element = {
			id : getNextId(),
			startDate : event.startDate,
			endDate : event.endDate,
			text : event.text
		};
		array.push(element);
		localStorage.setItem("calendarEvents", JSON.stringify(array));
	}
}

function generateEventHTML(array) {
	var html = '<p>' + selectedDay + '. ' + monthNames[calendarMonth] + '</p>';
	html += '<div id = "addEventSlot"></div>';
	if (array.length <= 0) {
		return html;
	}
	array.forEach(function(item) {
		html += '<div class = "calendarEvent" id = ';
		html += '"event' + item.id + '">';
		html += generateEventContent(item);
		html += '</div>';
	});
	return html;
}

function generateEventContent(event) {
	var html = '<div ondblclick="clickEvent(' + event.id + ')">';
	html += '<p>' + event.startDate.getHours() + ':' + event.startDate.getMinutes() + '-';
	html += event.endDate.getHours() + ':' + event.endDate.getMinutes() + '</p>';
	html += '<p>' + event.text + '</p>';
	html += '</div>';
	return html;
}

function generateEvent() {
	var fromHour = document.getElementById("fromDateHour").value;
	var fromMinute = document.getElementById("fromDateMinute").value;
	var toHour = document.getElementById("toDateHour").value;
	var toMinute = document.getElementById("toDateMinute").value;
	var input = document.getElementById("eventMessage").value;
	input = input.replace(/</g, "&lt;").replace(/>/g, "&gt;");
	var start = new Date();
	var end = new Date();

	start.setFullYear(calendarYear);
	start.setMonth(calendarMonth);
	start.setDate(selectedDay);
	start.setHours(fromHour);
	start.setMinutes(fromMinute);

	end.setFullYear(calendarYear);
	end.setMonth(calendarMonth);
	end.setDate(selectedDay);
	end.setHours(toHour);
	end.setMinutes(toMinute);

	if (start > end) {
		var event = {
			startDate : end,
			endDate : start,
			text : input
		};
		return event;
	} else {
		var event = {
			startDate : start,
			endDate : end,
			text : input
		};
		return event;
	}

}

function visualChangesWhenDayClicked(day) {
	//Remove selected from the class of the previous element that had it.
	var text = "selected";
	if (selectedDay) {

		document.getElementById("day" + selectedDay).classList.remove(text);
	}

	//Set the selected to the class of the clicked element.
	document.getElementById("day" + day).classList.add(text);
	//the new id has to be remembered so it can be disabled when
	//a new day is pressed.
	selectedDay = day;
}

function displayEventsOnDay(day) {
	//Create an array to store all the events that correspond with our date
	var array = [];
	getCalendarEvents().forEach(function(item) {
		var d = item.startDate;
		if (d.getFullYear() == calendarYear && d.getMonth() == calendarMonth && d.getDate() == day) {
			array.push(item);
		}
	});
	//Show today's events if any.
	var place = document.getElementById("calendarEvents");
	place.innerHTML = generateEventHTML(array);
	//display the add Event button
	cleanAddSlot();
}
//Display the event not the change event form.
function restoreOldEventState() {
	if (selectedEvent) {
		var place = document.getElementById("event" + selectedEvent.id);
		place.innerHTML = generateEventContent(selectedEvent);
	}
}


function clickNextMonth() {
	calendarMonth++;
	calendar();
}

function clickPrevMonth() {
	calendarMonth--;
	calendar();
}

function clickToday() {
	var date = new Date();
	calendarMonth = date.getMonth();
	calendarYear = date.getFullYear();
	calendar();
}

//When clicking on a day, the background changes and
//if there are events on that day, to display them.
function clickDay(day) {
	visualChangesWhenDayClicked(day);

	//Display the events of the selected day.
	displayEventsOnDay(day);

}

//Double clicking on an event makes it possible to change that event or delete it.
function clickEvent(id) {
	//Remove the old selectedEvent and create a new one holding the current event
	restoreOldEventState();
	selectedEvent = getCalendarEventWithId(id);
	var place = document.getElementById("event" + id);
	//add the form to modify and delete the that event.
	var html = '<form>';
	html += 'From: <input type="number" id="fromDateHour" min="0" max="23" value="' + selectedEvent.startDate.getHours() + '">';
	html += ':<input type="number" min="0" max="59" id="fromDateMinute" value="' + selectedEvent.startDate.getMinutes() + '"><br>';
	html += 'To: <input type="number" min="0" max="23" id="toDateHour" value="' + selectedEvent.endDate.getHours() + '">';
	html += ':<input type="number" min="0" max="59" id="toDateMinute" value="' + selectedEvent.endDate.getMinutes() + '"><br>';
	html += 'Message: <input type="text" id="eventMessage" value="' + selectedEvent.text + '"><br>';
	html += '<button onclick="changeEvent()">Save</button>';
	html += '<button onclick="restoreOldEventState()">Cancel</button>';
	html += '<button onclick="clickEventDelete()">Delete</button>';
	html += '</form>';
	place.innerHTML = html;
	//Add add new event button
	cleanAddSlot();
}

function changeEvent() {
	var generatedEvent = generateEvent();
	var eventId = selectedEvent.id;
	var eventWithId = {
		id : eventId,
		startDate : generatedEvent.startDate,
		endDate : generatedEvent.endDate,
		text : generatedEvent.text
	};
	updateCalendarEvent(eventWithId);
	selectedEvent = eventWithId;
	//refresh the calendar
	displayCalendar();
	//Display the new data for the events of this day.
	restoreOldEventState();
	clickDay(selectedDay);
}

function clickEventDelete() {
	//Delete the event
	deleteFromCalendarEvent(selectedEvent.id);
	//Empty the selection of the deleted event because it does not exist anymore
	selectedEvent = null;
	//refresh the calendar
	displayCalendar();
	//Display the new data for the events of this day.
	clickDay(selectedDay);
}

function clickAdd() {
	//Close old event changing form if it is open
	restoreOldEventState();
	var place = document.getElementById("addEventSlot");
	var html = '<form>';
	html += 'From: <input type="number" id="fromDateHour" min="0" max="23">';
	html += ':<input type="number" min="0" max="59" id="fromDateMinute"><br>';
	html += 'To: <input type="number" min="0" max="23" id="toDateHour">';
	html += ':<input type="number" min="0" max="59" id="toDateMinute"><br>';
	html += 'Message: <input type="text" id="eventMessage"><br>';
	html += '<button onclick="clickAddEvent()">Add</button>';
	html += '<button onclick="cleanAddSlot()">Cancel</button>';
	html += '</form>';
	place.innerHTML = html;
}

function cleanAddSlot() {
	var place = document.getElementById("addEventSlot");
	var html = '<button onclick="clickAdd()">Add</button>';
	place.innerHTML = html;
}

function clickAddEvent() {
	addCalendarEvent(generateEvent());
	displayCalendar();
	//Display the new data for the events of this day. "addElement"
	clickDay(selectedDay);
}